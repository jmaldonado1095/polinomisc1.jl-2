using FactCheck
import Polynomial
using PolinomisC1

include("helpers.jl")


tol = 1e-7

facts("Nivell bàsic") do

	
	context("Polinomis constants") do
		p = randompoly(0)
		@assert p[1] != 0

		@fact arrels(p) => empty
	end

	context("Polinomis lineals") do
		p = randompoly(1)

		@fact length(arrels(p)) => 1
		@fact peval(p, arrels(p)[1]) => roughly(0)
	end

	context("Polinomis de grau 2") do
		p = randomroots(2)
		xs = arrels(p)

		@fact length(xs) => 2
		@fact xs => are_roots_of(p)
	end

	context("Polinomis de grau 3") do
		p = randomroots(3)
		xs = arrels(p)

		@fact length(xs) => 3
		@fact xs => are_roots_of(p)
	end

end # facts