module PolinomisC1

using Polynomial

export arrels

arrels = p -> filter(isreal, roots(p))

end # module
